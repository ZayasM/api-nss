# Creación tabla Post
class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :text
    
      t.timestamps
    end
    
    add_foreign_key :posts, :users
  end
end
