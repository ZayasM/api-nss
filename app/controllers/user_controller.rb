# Usuario controlador
class UserController < ApplicationController
  def create
    @user = User.new(create_params)

    if @user.save
      render json: @user, status: 201
    else
      render json: { errors: model.errors }, status: 422
    end

  end

  def index
    @users = User.all

    render json: @users, status: 201
  end

  def create_params
    params.permit(:name, :email)
  end
end
